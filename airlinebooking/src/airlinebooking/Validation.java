package airlinebooking;

import java.util.regex.Pattern;

public class Validation {
	
	public boolean validationAirport(String valAp){
		
		//validera airport_code, endast 3 tecken och endast tecknen a - z och A - Z
		Pattern airportPattern = Pattern.compile("[a-zA-Z]{3}");
		return airportPattern.matcher(valAp).matches();
		
	}
	
	public boolean validationAirline(String valAline){
		
		//validera airline_name, endast 6 tecken och endast tecknen a - z och A - Z
		Pattern airlinePattern = Pattern.compile("[a-zA-Z]{6}");
		return airlinePattern.matcher(valAline).matches();
		
	}
}
