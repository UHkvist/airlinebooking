package airlinebooking;

public class Airport {
	
	private String code;
	private String name;
	
	//constructor Airport
	public Airport(String aCode, String aName){
		
		this.code = aCode;	
		this.name = aName;
	}	
	//---getter = h�mta och returnera
	
	public String getCode(){
		
		return this.code;		
	}
	public String getName(){
		
		return this.name;		
	}
	//--- setter = uppdatera, inget att returnera
	
	public void setCode(String aCode){
		
		this.code = aCode;	
	}
	public void setName(String aName){
			
		this.name = aName;		
	}
	//---
	
}
