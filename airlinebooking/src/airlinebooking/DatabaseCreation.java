package airlinebooking;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DatabaseCreation{

	public void createdb(){
			try {		
				
				//skapar databas airlinebookingsystem och 6 tabeller (airport, airline, plane, flightroute, passenger, booking)
				//get a connection to a database (airlinebookingsystem)
				Class.forName("com.mysql.jdbc.Driver");
				Connection MySQL56 = DriverManager.getConnection
						("jdbc:mysql://localhost:3306/","root","KaffeKopp063");		
				
				Statement myStmt = MySQL56.createStatement();
				
				myStmt.executeUpdate("CREATE DATABASE IF NOT EXISTS airlinebookingsystem DEFAULT CHARACTER SET utf8;");
				
				//TEST:System.out.println("TESTPUNKT EFTER DB creation");
				
				//create table AIRPORT
				myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS airlinebookingsystem.airport(id_airport int(11) NOT NULL AUTO_INCREMENT, "
						+ "airport_code varchar(3) NOT NULL UNIQUE, "
						+ "airport_location varchar (45) NOT NULL, "
						+ "PRIMARY KEY(id_airport));");
				
				//TEST:System.out.println("TESTPUNKT EFTER AIRPORT creation");
				
				//create table AIRLINE
				myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS airlinebookingsystem.airline(id_airline int(11) NOT NULL AUTO_INCREMENT, "
						+ "airline_name varchar(6) NOT NULL,"
						+ "PRIMARY KEY(id_airline));");
				
				//TEST:System.out.println("TESTPUNKT EFTER AIRLINE creation");
				
				//create table PLANE
				myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS airlinebookingsystem.plane(id_plane int(11) NOT NULL AUTO_INCREMENT, "
						+ "plane_id varchar(6) NOT NULL, "
						+ "PRIMARY KEY(id_plane));");
				
				//TEST:System.out.println("TESTPUNKT EFTER PLANE creation");
				
				//create table FLIGHTROUTE
				myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS airlinebookingsystem.flightroute(id_route int(11) NOT NULL AUTO_INCREMENT, "
						+ "dep varchar(3), dest varchar(3), flight_date varchar(8), id_airline int(11), id_plane int(11),"
						+ "PRIMARY KEY(id_route),"
						+ "CONSTRAINT flygbolag FOREIGN KEY(id_airline) REFERENCES airlinebookingsystem.airline(id_airline),"
						+ "CONSTRAINT flygplan FOREIGN KEY (id_plane) REFERENCES airlinebookingsystem.plane(id_plane));");
				
				//TEST:System.out.println("TESTPUNKT EFTER FLIGHTROUTE creation");
				
				//create table PASSENGER
				myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS airlinebookingsystem.passenger(id_passenger int(11) NOT NULL AUTO_INCREMENT,"
						+ "surname varchar(20), firstname varchar(20), email varchar(20), mobilenr varchar (15),"
						+ "PRIMARY KEY(id_passenger));");
				
				//TEST:System.out.println("TESTPUNKT EFTER PASSENGER creation");
				
				//create table BOOKING
				myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS airlinebookingsystem.booking (id_booking int(11) NOT NULL AUTO_INCREMENT,"
						+ "id_passenger int(11), id_route int(11),"
						+ "PRIMARY KEY(id_booking),"
				+ "CONSTRAINT passagerare FOREIGN KEY(id_passenger) REFERENCES airlinebookingsystem.passenger(id_passenger),"
				+ "CONSTRAINT flygrutt FOREIGN KEY (id_route) REFERENCES airlinebookingsystem.flightroute(id_route));");
				
				//TEST:System.out.println("TESTPUNKT EFTER BOOKING creation");
				
			}
			catch (ClassNotFoundException | SQLException exc){
				exc.printStackTrace();
				}
			


	}
}
