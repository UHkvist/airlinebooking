package airlinebooking;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;
import java.sql.SQLException;



public class Administrator {
	
	Scanner user_input = new Scanner(System.in);	
	Connection MySQL56;
	Statement myStmt;
	ResultSet myRs;
	
	Validation validate = new Validation();

	public Administrator(String myUrl, String myUser, String myPassword){
		
		//get a connection to database (airlinebookingsystem)
		try {
			MySQL56 = DriverManager.getConnection(myUrl,myUser,myPassword);
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	

	public void menyAdministrator(){
		
		int menySelection = 0;
		
		do{
			
			System.out.println("1  = Skapa flygplats");	
			System.out.println("2  = Skapa flygbolag");	
			System.out.println("3  = Skapa flygplan");	
			System.out.println("4  = Skapa flyglinje");
			System.out.println("5  = Lista alla bokningar");
			System.out.println("6  = Lista alla bokningar f�r vald passagerare");
			System.out.println("7  = Lista alla flyglinjer");
			System.out.println("8  = Lista alla flyglinjer per flygbolag");
			System.out.println("9  = till Startmenyn");
			System.out.println("10 = till LOGIN");
			
			menySelection = user_input.nextInt();
			
			
			switch(menySelection){
			case 1:
				createAirport();
				break;
				
			case 2:
				createAirline();
				break;
			
			case 3:
				createPlane();
				break;
				
			case 4:
				createFlightroute();
				break;
				
			case 5:
				listAllBookings();
				break;	
				
			case 6:
				listAllBookingsPassenger();
				break;		
			
			case 7:
				listAllRoutes();
				break;		
			
			case 8:
				listAllRoutesAirline();
				break;	
				
			case 9:
				break;
				//exit loop till startmeny
			}
			
			
		}while(!(menySelection>=10));
		
		}
	

	public void createAirport(){
		
		String valApcode = null;
		
		do{
			System.out.println("Flygplatskod (3 bokst�ver) : ");
			valApcode = user_input.next();
			if (!validate.validationAirport(valApcode)){
				System.out.println("ogiltig flygplatskod, f�rs�k igen");
			}
			
		}while (!validate.validationAirport(valApcode));
		
		System.out.println("Flygplatsort (utan mellanslag): ");	
		String ap_loc = user_input.next();
		
		try{
			
		
			myStmt = MySQL56.createStatement();	
			
		Airport newairport = new Airport(valApcode, ap_loc);
					
		newairport.setCode(valApcode);
		newairport.setName(ap_loc);
		
		myStmt.executeUpdate("INSERT IGNORE INTO airlinebookingsystem.airport(airport_code, airport_location) VALUES  ('"+valApcode+"', '"+ap_loc+"');");
		
	//end of try
		}
	
		catch (Exception exc){
			exc.printStackTrace();
			}
	}
	
	
	public void createAirline(){

		
		String valAlinename = null;
		
		do{
			System.out.println("Flygbolag (6 bokst�ver) : ");
			valAlinename = user_input.next();
			if (!validate.validationAirline(valAlinename)){
				System.out.println("ogiltigt flygbolagsnamn, f�rs�k igen");
			}
			
		}while (!validate.validationAirline(valAlinename));
		
		try{
							
				myStmt = MySQL56.createStatement();	
				
		//skapa flygbolag	
		Airline newairline = new Airline(valAlinename);
		newairline.setName(valAlinename);
		
		myStmt.executeUpdate("INSERT IGNORE INTO airlinebookingsystem.airline (airline_name) VALUES  ('"+valAlinename+"');");
		
		
		System.out.println("Flygbolag:  ");
		
		ResultSet myRs = myStmt.executeQuery("SELECT * from airlinebookingsystem.airline");
	
	while (myRs.next()){
		System.out.println(myRs.getString("id_airline")+" "+myRs.getString("airline_name"));
		}
		//end try
		}
	
		catch (Exception exc){
			exc.printStackTrace();
			}
	}
		
	//---
	public void createPlane(){
	// ??? Plane subklass till Airline????
	int valtplane_id;	
	System.out.println("V�lj flygplansid :");
	valtplane_id = user_input.nextInt();
	
	//skapa flygplan	
	Plane newplane = new Plane(valtplane_id);
	newplane.setPlane(valtplane_id);

	
	try{
			
		myStmt = MySQL56.createStatement();	
		
		myStmt.executeUpdate("INSERT IGNORE INTO airlinebookingsystem.plane (plane_id) VALUES  ('"+valtplane_id+"');");
		
	//end try
	}
	catch (Exception exc){
		exc.printStackTrace();
		}
	}
	
	public void createFlightroute(){
		
		//
		
		System.out.println("V�lj avg�ngsflygplats med siffra:");
		String dep_airport = user_input.next();
		
		System.out.println("V�lj destinationsflygplats med siffra:");
		String dest_airport = user_input.next();
		
		System.out.println("V�lj datum:");
		String route_date = user_input.next();
		
		System.out.println("V�lj flygbolag med siffra:");
		int route_airline = user_input.nextInt();
		
		System.out.println("V�lj flygplan med siffra:");
		int route_plane = user_input.nextInt();
		
		Flightroute newflight = new Flightroute(dep_airport, dest_airport, route_date, route_airline, route_plane);
		
		newflight.setDep(dep_airport);
		newflight.setDest(dest_airport);
		newflight.setFlightDate(route_date);
		newflight.setIdAirline(route_airline);
		newflight.setPlaneid(route_plane);
		
		try{
			
			myStmt = MySQL56.createStatement();
			
		myStmt.executeUpdate("INSERT IGNORE INTO airlinebookingsystem.flightroute (dep, dest, flight_date, id_airline, id_plane) VALUES  ('"+dep_airport+"', '"+dest_airport+"', '"+route_date+"', '"+route_airline+"', '"+route_plane+"');");
		//end try
		}
		catch (Exception exc){
			exc.printStackTrace();
			}
	}
	
	//---
	public void listAllBookings(){
		
		try{
				
			myStmt = MySQL56.createStatement();		
			
			
		ResultSet myRs = myStmt.executeQuery("SELECT * from airlinebookingsystem.booking");
		
		while (myRs.next()){
			System.out.print("Bokningsnummer: ");
			System.out.println(myRs.getString("id_booking")+" Kundnummer: "+myRs.getString("id_passenger"));
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
			}
	}
	
	//---
	public void listAllBookingsPassenger(){
		
	// s�k p� passagerare
		System.out.println("V�lj bokningar f�r enskild passagerare,ange kundnummer :");
		String pass_id = user_input.next();
		
		try{
				
			myStmt = MySQL56.createStatement();	
			
			ResultSet myRs = myStmt.executeQuery("SELECT * from airlinebookingsystem.booking where id_passenger = '"+pass_id+"'");
			
			while (myRs.next()){
				System.out.print("Bookningsnummer: ");
				System.out.println(myRs.getString("id_booking")+" Kundnummer:  "+myRs.getString("id_passenger"));
				}
			
		//end try
		}
		catch (Exception exc){
			exc.printStackTrace();
			}
	}
	
	//---
	public void listAllRoutes(){
		try{
			System.out.println("Alla flyglinjer ->");
			
			myStmt = MySQL56.createStatement();	
			
			ResultSet myRs = myStmt.executeQuery("SELECT * from airlinebookingsystem.flightroute");
			
			while (myRs.next()){
				System.out.print("Flyglinjenr: ");
				System.out.println(myRs.getString("id_route")+" Dep: "+myRs.getString("dep")+" Dest: "+myRs.getString("dest")+" Flygdatum: "+myRs.getString("flight_date")+" Flygbolag: "+myRs.getString("id_airline")+" Flygplansnr: "+myRs.getString("id_plane"));
				}
			
		//end try
		}
		catch (Exception exc){
			exc.printStackTrace();
			}
	}
	//---
	public void listAllRoutesAirline(){
		try{
			System.out.println("Lista flyglinjer f�r ett flygbolag:");
			int list_airline = user_input.nextInt();
			System.out.println("Flygbolagnr:"+list_airline);
			
			myStmt = MySQL56.createStatement();	
			
			ResultSet myRs = myStmt.executeQuery("SELECT * from airlinebookingsystem.flightroute where id_airline = '"+list_airline+"'");
			
			while (myRs.next()){
				System.out.print("Flyglinjenr: ");
				System.out.println(myRs.getString("id_route")+" Dep: "+myRs.getString("dep")+" Dest: "+myRs.getString("dest")+" Flygdatum: "+myRs.getString("flight_date"));
				}
			
		//end try
		}
		catch (Exception exc){
			exc.printStackTrace();
			}
	}
	//---
	
}

				
	
	
	
	
	
	



