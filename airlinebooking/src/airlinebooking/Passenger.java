package airlinebooking;

import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;


public class Passenger {
	
	Scanner user_input = new Scanner(System.in);		
	Connection MySQL56;
	Statement myStmt;
	ResultSet myRs;
	
	public Passenger(String myUrl, String myUser, String myPassword){
	
		try {
			//get a connection to database (airlinebookingsystem)
			MySQL56 = DriverManager.getConnection(myUrl,myUser,myPassword);
			
		} catch (SQLException e) {	
			e.printStackTrace();
		}		
	}
	
	public void menyPassenger(){
			
			int menySelection = 0;
			
			do{
				System.out.println("1  = Kundregistrering");
				System.out.println("2  = S�k flygavg�ngar");	
				System.out.println("3  = Boka flygbiljett");	
				System.out.println("4  = Se bokningar");	
				System.out.println("5  = tillbaka till Startmenyn");
				
				
				menySelection = user_input.nextInt();
				
				
				switch(menySelection){
				
				case 1:
					regPassenger();
					break;
					
				case 2:
					searchDestDep();
					break;
					
				case 3:
					bookTicket();
					break;
				
				case 4:
					listBookings();
					break;
					
				case 5:
					break;
					//tillbaka till startmenyn
				}
				
				
			}while(!(menySelection>=5));
			
			}

		//---
	
		public void regPassenger(){
			
			System.out.println("Efternamn: ");	
			String pass_surname = user_input.next();
			
			System.out.println("F�rnamn: ");	
			String pass_firstname = user_input.next();
			
			System.out.println("Email: ");	
			String pass_email = user_input.next();
			
			System.out.println("Mobile nr: ");	
			String pass_mobilenr = user_input.next();
			
			
			
			try{	
			
				myStmt = MySQL56.createStatement();	
				
				myStmt.executeUpdate("INSERT IGNORE INTO airlinebookingsystem.passenger (surname, firstname, email, mobilenr) VALUES  ('"+pass_surname+"', '"+pass_firstname+"', '"+pass_email+"', '"+pass_mobilenr+"');");
			//end try	
			}
			catch (Exception exc){
				exc.printStackTrace();
				}
			
			
		}
		
		public void searchDestDep(){
			
			try{
		
			myStmt = MySQL56.createStatement();
			
			
			ResultSet myRs = myStmt.executeQuery("SELECT * from airlinebookingsystem.flightroute");
			
			while (myRs.next()){
				System.out.print("Flyglinje:");
				System.out.println(myRs.getString("id_route")+" Dep:"+myRs.getString("dep")+" Dest:"+myRs.getString("dest")+" Datum:"+myRs.getString("flight_date"));
			}
			//end try
			}
			catch (Exception exc){
				exc.printStackTrace();
				}
			
		
		}	
		public void bookTicket(){
				
			try{
				
				myStmt = MySQL56.createStatement();
				
				System.out.println("Kundnummer passagerare: ");	
				int booking_kundnr = user_input.nextInt();
				
				//kontroll att kundnummer finns
				//myRs = myStmt.executeQuery("SELECT COUNT(id_passenger) AS pass from airlinebookingsystem.passenger WHERE (id_passenger ='"+booking_kundnr+"')");
				//String errormsg_1 = "Kundnummer finns inte!";
				//myRs= myStmt.executeQuery("SELECT IF(pass=0, 'true', 'false') AND IF 'true' PRINT('"+errormsg_1+"')");  
							
				
				System.out.println("Tillg�ngliga flyglinjer: ");	
				
				ResultSet myRs = myStmt.executeQuery("SELECT * from airlinebookingsystem.flightroute");
				
				while (myRs.next()){
					System.out.print("Flyglinjer:");
					System.out.println(myRs.getString("id_route")+" Dep:"+myRs.getString("dep")+" Dest:"+myRs.getString("dest")+" Datum:"+myRs.getString("flight_date"));
				}
							
				System.out.println("V�lj flyglinje: ");	
				int booking_route = user_input.nextInt();
				
				
				//kontroll att flyglinje finns
				//myRs = myStmt.executeQuery("SELECT COUNT(id_route) AS flights from airlinebookingsystem.flightroute WHERE (id_route ='"+booking_route+"')");
				//String errormsg_2 = "Vald flyglinje finns inte!";
				//myRs= myStmt.executeQuery("SELECT IF(flights=0, 'true', 'false') AND IF 'true' PRINT('"+errormsg_2+"')");  
				
				myStmt.executeUpdate("INSERT IGNORE INTO airlinebookingsystem.booking(id_passenger, id_route) VALUES  ('"+booking_kundnr+"', '"+booking_route+"');");
				
			}
			
				catch (Exception exc){
					exc.printStackTrace();
					}
			
		}
		
		
		public void listBookings(){
			
		try{
			
			myStmt = MySQL56.createStatement();
			
			System.out.print("Ange bokningsnummer: ");	
			int bookingnumber = user_input.nextInt();
			
			ResultSet myRs = myStmt.executeQuery("SELECT id_booking, id_passenger FROM airlinebookingsystem.booking WHERE id_booking = '"+bookingnumber+"'");
			
			while (myRs.next()){
				System.out.print("Bokningsnummer: ");
				System.out.println(myRs.getString("id_booking")+"  Kundnummer: "+myRs.getString("id_passenger"));
				System.out.println("");
			}
			
			//end try
			}
		
			catch (Exception exc){
				exc.printStackTrace();
				}
		}
		
	}
		


