package airlinebooking;

public class Flightroute {
	
	private String dep;
	private String dest;
	private String flight_date;
	private int id_airline;
	private int plane_id;
	
	//constructor Flightroute
	public Flightroute(String frDep, String frDest, String frFlightDate, int frIdairline, int frPlaneId){
		
		this.dep = frDep;	
		this.dest = frDest;
		this.flight_date = frFlightDate;
		this.id_airline = frIdairline;
		this.plane_id = frPlaneId;
	}

	//---getter = h�mta och returnera
	public String getDep(){
		return this.dep;
	}
	
	public String getDest(){
		return this.dest;
	}
	
	public String getFlightDate(){
		return this.flight_date;	
	}	
	
	public int getIdairline(){
		return this.id_airline;
	}
	
	public int getPlaneid(){
		return this.plane_id;
	}
	
	//--- setter = uppdatera, inget att returnera
	
	public void setDep(String frDep){
		this.dep = frDep;
	}
	
	public void setDest(String frDest){
		this.dest = frDest;	
	}
	
	public void setFlightDate(String frFlightDate){
		this.flight_date = frFlightDate;
	}
	
	public void setIdAirline(int frIdairline){
		this.id_airline = frIdairline;
	}
	
	public void setPlaneid(int frPlaneId){
		this.id_airline = frPlaneId;
	}
	//---
}
	

	

	
	




